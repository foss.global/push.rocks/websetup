import * as plugins from './websetup.plugins.js';
import * as interfaces from './interfaces/index.js';

import { Tag } from './websetup.classes.tag.js';

export class JsonLdTag extends Tag {
  public static createCompanyJsonLd(companyDataArg: plugins.tsclass.business.ICompany) {
    // STATIC
    // lets care about linked data
    const companyLd = {
      '@context': 'https://schema.org',
      '@type': 'Corporation',
      name: companyDataArg.name,
      alternateName: companyDataArg.name.replace(' GmbH', ''),
      url: companyDataArg.contact.website,
      logo: companyDataArg.contact.logoUrl,
      contactPoint: {
        '@type': 'ContactPoint',
        telephone: companyDataArg.contact.phone,
        contactType: 'customer service',
        areaServed: 'DE',
        availableLanguage: ['en', 'German'],
      },
      sameAs: [],
    };

    if (companyDataArg.contact.facebookUrl) {
      companyLd.sameAs.push(companyDataArg.contact.facebookUrl);
    }

    if (companyDataArg.contact.twitterUrl) {
      companyLd.sameAs.push(companyDataArg.contact.twitterUrl);
    }

    const ldTag = new JsonLdTag(companyLd);
    return ldTag;
  }

  public static createNewsArticleJsonLd(newsArticleArg: plugins.tsclass.content.IArticle) {
    const newsArticleLd = {
      '@context': 'https://schema.org',
      '@type': 'NewsArticle',
      mainEntityOfPage: {
        '@type': 'WebPage',
        '@id': window.location.href,
      },
      headline: 'Article headline',
      image: [newsArticleArg.featuredImageUrl],
      datePublished: new Date(newsArticleArg.timestamp).toISOString(),
      dateModified: new Date(newsArticleArg.timestamp).toISOString(),
      author: {
        '@type': 'Person',
        name: `${newsArticleArg.author.firstName} ${newsArticleArg.author.surName}`,
      },
      publisher: {
        '@type': 'Organization',
        name: newsArticleArg.author.surName, // TODO
        logo: {
          '@type': 'ImageObject',
          url: newsArticleArg.author.surName, // TODO
        },
      },
      description: newsArticleArg.author.firstName,
    };
    const ldTag = new JsonLdTag(newsArticleLd);
    return ldTag;
  }
  public static createProductJsonLd(
    productArg: plugins.tsclass.saas.IProduct,
    publisherArg: plugins.tsclass.business.ICompany
  ) {
    const productLd = {
      '@context': 'https://schema.org',
      '@type': 'SoftwareApplication',
      name: productArg.name,
      description: productArg.description,
      operatingSystem: productArg.os,
      applicationCategory: productArg.category,
      offers: {
        '@type': 'Offer',
        name: 'User-based Plan',
        priceSpecification: {
          '@type': 'PropertyValueSpecification',
          valueName: 'Number of Users',
          valueRequired: true,
          price: '4.99',
          priceCurrency: 'EUR',
        },
      },
      publisher: this.createCompanyJsonLd(publisherArg).elementRef.textContent,
      screenshot: 'https://www.social.io/screenshot.png',
      url: 'https://www.social.io/',
    };
    const ldTag = new JsonLdTag(productLd);
    return ldTag;
  }

  // INSTANCE
  constructor(ldObjectArg: any) {
    super();
    const jsonLdElement = document.createElement('script');
    jsonLdElement.type = 'application/ld+json';
    jsonLdElement.text = JSON.stringify(ldObjectArg);
    this.elementRef = jsonLdElement;
  }
}
