import * as plugins from './websetup.plugins.js';
import { Tag } from './websetup.classes.tag.js';

export class OpengraphTag extends Tag {
  public static createNewsArticleOgTags(newsArticleArg: plugins.tsclass.content.IArticle) {
    const tagArray: OpengraphTag[] = [];
    tagArray.push(new OpengraphTag('og:url', newsArticleArg.url));
    tagArray.push(new OpengraphTag('og:title', newsArticleArg.title));
    tagArray.push(new OpengraphTag('og:description', newsArticleArg.content));
    tagArray.push(new OpengraphTag('og:image', newsArticleArg.featuredImageUrl));
    return tagArray;
  }

  public static createProductOgTags(productArg: plugins.tsclass.saas.IProduct) {
    const tagArray: OpengraphTag[] = [];
    tagArray.push(new OpengraphTag('og:url', globalThis.location.href));
    tagArray.push(new OpengraphTag('og:title', `${productArg.name} - ${productArg.slogan}`));
    tagArray.push(new OpengraphTag('og:site_name', productArg.name))
    tagArray.push(new OpengraphTag('og:description', productArg.description));
    tagArray.push(new OpengraphTag('og:image', productArg.logoLink));
    return tagArray;
  }

  constructor(propertyNameArg: string, contentArg: string) {
    super();
    const openGraphElement = document.createElement('meta');
    openGraphElement.setAttribute('property', propertyNameArg);
    openGraphElement.content = contentArg;
    this.elementRef = openGraphElement;
  }
}
