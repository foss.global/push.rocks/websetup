import * as plugins from '../websetup.plugins.js';
export interface IMetaObject {
  title: string;
  description?: string;
  canonicalDomain?: string;
  ldCompany?: plugins.tsclass.business.ICompany;
  ldProduct?: plugins.tsclass.saas.IProduct;

  // handles
  twitterHandle?: string;
  facebookHandle?: string;

  // links
  companyWebsiteLink?: string;
  googleNewsLink?: string;
  mediumLink?: string;
  slackLink?: string;
  airmeetLink?: string;
}
