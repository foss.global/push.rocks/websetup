import * as plugins from './websetup.plugins.js';

/**
 * a title proxy class
 * --> to be used in the future when flashing titles is supported
 */
export class Title {}
