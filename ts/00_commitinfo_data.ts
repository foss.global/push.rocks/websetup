/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/websetup',
  version: '3.0.19',
  description: 'setup basic page properties'
}
