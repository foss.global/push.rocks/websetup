// pushrocks scope
import * as smartdelay from '@pushrocks/smartdelay';
import * as smartpromise from '@pushrocks/smartpromise';

export { smartdelay, smartpromise };

// tsclass scope
import * as tsclass from '@tsclass/tsclass';

export { tsclass };
