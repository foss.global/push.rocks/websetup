import { expect, tap, webhelpers } from '@pushrocks/tapbundle';
webhelpers.enable();

import * as websetup from '../ts/index.js';

tap.test('first test', async () => {
  const websetupInstance = new websetup.WebSetup({
    metaObject: {
      description: 'A awesome description',
      title: 'mytitle',
      canonicalDomain: 'lossless.com',
    },
  });
  await websetupInstance.readyPromise;
  expect(document.title).toEqual('mytitle');
});

tap.start();
